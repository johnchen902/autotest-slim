Usage
===

    python3 autotest.py a.cpp input.txt

    # On another terminal
    vim input.txt
    vim a.cpp # compile and test while you edit

Sample input.txt
---

    '10 20'
    '30 40'
    '1 7121'
    '%d %d' % (2**28, 2**28)
    str1(2**29, 2**29)
    str1(2**30, 2**30)

Sample a.cpp
---

    #include <iostream>
    int main() {
        int x, y;
        std::cin >> x >> y;
        std::cout << x + y << std::endl;
    }

Dependencies
===
None
