#!/usr/bin/env python3
import os, subprocess, sys, time, traceback
def str1(*x): return ' '.join(str(y) for y in x)
def banner(x): print((' ' + x + ' ').center(80, '\u2550'))
s1, s2 = sys.argv[1], sys.argv[2]
t1 = t2 = None
while True:
    while True:
        try:
            t3 = os.path.getmtime(s1)
            t4 = os.path.getmtime(s2)
            if t1 != t3 or t2 != t4:
                t1, t2 = t3, t4
                break
        except:
            pass
        time.sleep(1)
    banner("Compiling")
    cmd = 'g++ -O2 -g -fsanitize=undefined -Wall -Wextra -Wshadow -Wno-unused-result -std=c++17'
    if subprocess.run(cmd.split() + [s1]).returncode:
        continue
    banner("Reading input")
    try:
        testcase = [eval(s).encode() for s in open(s2)]
    except Exception:
        traceback.print_exc()
        continue
    for i, tc in enumerate(testcase):
        banner("Testing input %d" % i)
        rc = subprocess.run('./a.out', input=tc).returncode
        if rc:
            banner("Return code %d" % rc)
            break
    else:
        banner("Done")
